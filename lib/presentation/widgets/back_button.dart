import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ButtonBack extends StatelessWidget {
  const ButtonBack({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        height: 44.w,
        width: 44.w,
        decoration: BoxDecoration(
          color: Colors.white,

          border: Border.all(color: Colors.transparent),
          borderRadius: BorderRadius.circular(40)
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
          child: SvgPicture.asset('assets/back.svg'),
        ),
      ),
    );
  }
}
