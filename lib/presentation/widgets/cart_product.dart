import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CardProduct extends StatefulWidget {
  bool isFavourite;
  bool isInBasket;
  final Function()? onTap;


  CardProduct({super.key, this.onTap, required this.isFavourite, required this.isInBasket});

  @override
  State<CardProduct> createState() => _CardProductState();
}

class _CardProductState extends State<CardProduct> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16)
      ),
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 12.w, top: 12.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.w),
                  child: SizedBox(
                      width: double.infinity,
                      child: AspectRatio(
                        aspectRatio: 114.98/53.85,
                        child: Image.asset('assets/nike-zoom-winflo-3-831561-001-mens-running-shoes-11550187236tiyyje6l87_prev_ui 1.png', fit: BoxFit.cover,),
                      )
                  ),
                ),
                SizedBox(height: 16.w,),
                Text(
                  'Best Seller',
                  style: TextStyle(
                      fontSize: 12.sp,
                      height: 16/12.w,
                      color: Color(0xFF48B2E7)
                  ),
                ),
                SizedBox(height: 8.w,),
                Text(
                  'Nike Air Max',
                  style: TextStyle(
                      fontSize: 16.sp,
                      height: 20/16.w,
                      color: Color(0xFF6A6A6A)
                  ),
                  maxLines: 1,
                ),
                Expanded(child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(child: Text(
                        '₽752.00',
                        style: TextStyle(
                            fontSize: 14.sp,
                            height: 16/14.w,
                            color: Color(0xFF6A6A6A)
                        ),
                      )),
                      GestureDetector(
                        child: Container(
                          height: 35.5.w,
                          width: 34.w,
                          decoration: BoxDecoration(
                              color: Color(0xFF48B2E7),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(14.w),
                                  bottomRight:Radius.circular(14.w)
                              )
                          ),
                          padding: EdgeInsets.all(10.w),
                          child: SvgPicture.asset('assets/bag.svg'),
                        ),
                      )
                    ],
                  ),
                ))

              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 9.w, top: 3.w),
            child: Container(
              height: 28.w,
              width: 28.w,
              padding: EdgeInsets.all(6),
              decoration: BoxDecoration(
                color: Color(0xFFF7F7F9),
                borderRadius: BorderRadius.circular(40),
                border: Border.all(color: Colors.transparent)
              ),
              child: SvgPicture.asset('assets/heart_yes.svg'),
            ),
          )
        ],
      )

    );
  }
}
