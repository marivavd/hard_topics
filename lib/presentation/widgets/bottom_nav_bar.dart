import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BottomNavigBar extends StatefulWidget {
  final Function(int) onSelect;
  final Function() onTapCart;
  const BottomNavigBar({super.key, required this.onSelect, required this.onTapCart});

  @override
  State<BottomNavigBar> createState() => _BottomNavigBarState();
}

class _BottomNavigBarState extends State<BottomNavigBar> {

  var currentIndex = 0;

  void onTap(int newIndex){
    setState(() {
      currentIndex = newIndex;
    });
    widget.onSelect(currentIndex);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 106.w,
      width: double.infinity,
      child: Stack(
        children: [
          Transform.translate(offset: Offset(-1.5.w, 0),
            child: ImageFiltered(
                imageFilter: ImageFilter.blur(sigmaY: 4, sigmaX: 4),
            child: SizedBox(
              width: double.infinity,
              child: SvgPicture.asset('assets/background.svg',
              fit: BoxFit.fill, color: Color(0x1F83AAD1)),

            ),),

          ),
          Transform.translate(offset: Offset(0, 4.w),
            child: ImageFiltered(
              imageFilter: ImageFilter.blur(sigmaY: 15, sigmaX: 15),
              child: SizedBox(
                width: double.infinity,
                child: SvgPicture.asset('assets/background.svg',
                    fit: BoxFit.fill, color: Color(0x26000000)),

              ),),

          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              width: double.infinity,
              height: 106.w,
              child: SvgPicture.asset('assets/background.svg',
                  fit: BoxFit.fill),

            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 30.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        child: SvgPicture.asset('assets/home.svg',
                        width: 24.w,
                            height: 24.w,
                            color: (currentIndex == 0)?Color(0xFF48B2E7):Color(0xFF707B81)
                        ),
                        onTap: () => onTap(0),
                      ),
                      SizedBox(width: 40.w,),
                      GestureDetector(
                        child: SvgPicture.asset('assets/heart.svg',
                            width: 24.w,
                            height: 24.w,
                            color: (currentIndex == 1)?Color(0xFF48B2E7):Color(0xFF707B81)
                        ),
                        onTap: () => onTap(1),
                      ),
                    ],
                  ),
                  SizedBox(width: 138.w,),
                  Row(
                    children: [
                      GestureDetector(
                        child: SvgPicture.asset('assets/notification.svg',
                            width: 24.w,
                            height: 24.w,
                            color: (currentIndex == 2)?Color(0xFF48B2E7):Color(0xFF707B81)
                        ),
                        onTap: () => onTap(2),
                      ),
                      SizedBox(width: 40.w,),
                      GestureDetector(
                        child: SvgPicture.asset('assets/profile.svg',
                            width: 24.w,
                            height: 24.w,
                            color: (currentIndex == 3)?Color(0xFF48B2E7):Color(0xFF707B81)
                        ),
                        onTap: () => onTap(3),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: GestureDetector(
              onTap: widget.onTapCart,
              child: Padding(
                padding: EdgeInsets.only(bottom: 50.w),
                child: Container(
                  height: 56.w,
                  width: 56.w,
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: Color(0xFF48B2E7),
                    borderRadius: BorderRadius.circular(30.w),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 24,
                        color: Color(0x995B9EE1),
                        offset: Offset(0, 8.w)
                      )
                    ],
                    
                  ),
                  child: SvgPicture.asset('assets/cart.svg'),
                ),
              ),

            ),
          )
        ],
      ),
    );
  }
}
