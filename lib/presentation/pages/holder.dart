import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hard_topics/presentation/widgets/back_button.dart';
import 'package:hard_topics/presentation/widgets/cart_product.dart';

class Holder extends StatefulWidget {
  const Holder({super.key});

  @override
  State<Holder> createState() => _HolderState();
}

class _HolderState extends State<Holder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 100,),
            ButtonBack(),
            InkWell(
              child: Container(
                height: 44.w,
                width: 44.w,
                decoration: BoxDecoration(
                    color: Colors.white,

                    border: Border.all(color: Colors.transparent),
                    borderRadius: BorderRadius.circular(40)
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                  child: SvgPicture.asset('assets/back.svg'),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
