import 'package:barcode/barcode.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ShtrixCode extends StatefulWidget {
  const ShtrixCode({super.key});

  @override
  State<ShtrixCode> createState() => _ShtrixCodeState();
}

Widget buildStrix(
    Barcode bc,
    String data,
{
  double? width,
  double? height,
  double? fontHeight
}
    ){
  final svg = bc.toSvg(
    data,
    width: width ?? 100.w,
    height: height ?? 100.w,
    fontHeight: fontHeight
  );
  return SvgPicture.string(svg);
}

class _ShtrixCodeState extends State<ShtrixCode> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildStrix(Barcode.pdf417(), 'msma;las;mf;as'),
    );
  }
}
