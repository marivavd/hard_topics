import 'dart:typed_data';

import 'package:image_picker/image_picker.dart';

class AvatarUseCase{
  XFile? avatar;
  final Future<ImageSource?> Function() chooseSource;
  final Function(Uint8List) pickAvatar;
  final Function() removeAvatar;

  AvatarUseCase({required this.chooseSource, required this.pickAvatar, required this.removeAvatar});

  void pressButton(flag){
    if (flag){

    }
    else{
      pressChangeButton();
    }
  }

  Future<void> pressChangeButton()async{
    var source = await chooseSource();
    if (source == null){
      return;
    }
    avatar = await ImagePicker().pickImage(source: source);
    var bytes = await avatar?.readAsBytes();
    if (bytes != null){
      pickAvatar(bytes);
    }
  }
  void pressRemoveButton(){
    avatar = null;
    removeAvatar();
  }



}